import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import ListContact from '../ListContact';
import AddContact from '../AddContact';

const ContactListStack = createStackNavigator();
const ContactList = () => {
    return(
        <ContactListStack.Navigator>
            <ContactListStack.Screen
                name='ListContact'
                component={ListContact}
                options={{
                    headerTitle: 'List Contacts',
                    headerTitleAlign: 'center',
                }}
            />
            <ContactListStack.Screen
                name='UpdateContact'
                component={AddContact}
                options={{
                    headerTitle: 'Update Contact',
                    headerTitleAlign: 'center'
                }}
            />
        </ContactListStack.Navigator>
    )
}

const ContactAddStack = createStackNavigator();
const ContactAdd = () => {
    return(
        <ContactAddStack.Navigator>
            <ContactAddStack.Screen
                name='AddContact'
                component={AddContact}
                options={{
                    headerTitle: 'Add Contact',
                    headerTitleAlign: 'center'
                }}
            />
        </ContactAddStack.Navigator>
    )
}

const Tab = createBottomTabNavigator();

const Menu = () => {
    return (
        <Tab.Navigator initialRouteName='ListContact' tabBarOptions={{showLabel:false, style:{...styles.tabBarStyle}}}>
            <Tab.Screen 
                name='ListContact' 
                component={ContactList} 
                options={{
                    tabBarIcon : ({focused}) => (
                        <View style={styles.iconMenuBar}>
                            <Image
                                source={require('../../assets/images/contact-menu.png')}
                                resizeMode='contain'
                                style={styles.imageMenuBar}
                            />
                            <Text style={{color: focused ? 'black' : 'white', ...styles.textStyle}}>Contact</Text>
                        </View>
                    ),
                }}
            />
            <Tab.Screen 
                name='AddContact' 
                component={ContactAdd}
                options={{
                    tabBarIcon : ({focused}) => (
                        <View style={styles.iconMenuBar}>
                            <Image
                                source={require('../../assets/images/add-menu.png')}
                                resizeMode='contain'
                                style={{tintColor: '#4ca1a3', ...styles.imageMenuBar}}
                            />
                            <Text style={{color: focused ? 'black' : 'white', ...styles.textStyle}}>Add</Text>
                        </View>
                    ),
                }}
            />
      </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    wrapper:{flex:1, justifyContent:'center',alignItems: 'center'},
    iconMenuBar:{alignItems:'center', justifyContent:'center'},
    textStyle:{textAlign:'center', fontWeight:'bold', fontSize:12},
    tabBarStyle:{height:80, backgroundColor:'skyblue'},
    imageMenuBar:{height:40, width:40}
})

export default Menu;
