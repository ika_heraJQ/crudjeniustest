import axios from 'axios'
import React, { useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Modal, Pressable} from 'react-native'
import { useNavigation, useFocusEffect } from '@react-navigation/native'

const ListContact = () => {
    const navigation = useNavigation();
    const [contact, setContact] = useState([]);
    const [popUpVisible, setPopUpVisible] = useState(false);
    const [msgErr, setMsgErr] = useState('');

    const getData = () => {
        axios.get('https://simple-contact-crud.herokuapp.com/contact')
        .then(res => {
            console.log('res get data : ' + res);
            setContact(res.data.data);
        })
        .catch(err => console.log('err get data : ', err));
    }

    const GoToDetail = (item) => {
        axios.get(`https://simple-contact-crud.herokuapp.com/contact/${item.id}`)
        .then(res => {
            console.log('res get data by id : ' + res);
            navigation.navigate('UpdateContact', {screen:'AddContact', params : res.data.data})
        })
        .catch(err => {
            console.log('err get data : ', err)
            setPopUpVisible(true);
            setMsgErr(err.toString());
        });
    }

    const DeleteItem = (item) => {
        console.log('item deleted : ', item);
        axios.delete(`https://simple-contact-crud.herokuapp.com/contact/${item.id}`)
        .then(res => {
            console.log('res deleted data : ', res);
            getData();
            setPopUpVisible(true);
            setMsgErr(res.data.message.toString()); 
        })
        .catch(err => {
            console.log('err delete data : ', err);
            setPopUpVisible(true);
            setMsgErr(err.toString());            
        });
    }

    useFocusEffect(
        React.useCallback(() => {
            //console.log('focus');
            getData();

            return () => {
                
            };
        }, [])
    )

    return (
        <View>
            <ScrollView>
                {contact.map(con => {
                    return <ListItem key={con.id} urlAvatar={con.photo} firstName={con.firstName} lastName={con.lastName} age={con.age} deleteItem={() => DeleteItem(con)} toDetail={() => GoToDetail(con)}/>
                })}
            </ScrollView>
            <Modal
                animationType='fade'
                transparent={true}
                visible={popUpVisible}
                onRequestClose={
                    () => {
                        Alert.alert('Modal has been closed');
                        setPopUpVisible(!popUpVisible);
                    }
                }
            >
                <View style={styles.wrapperPopUp}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>{msgErr}</Text>
                        <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => setPopUpVisible(!popUpVisible)}
                        >
                        <Text style={styles.textStyle}>OK</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View>        
    )
}

const ListItem = ({urlAvatar, firstName, lastName, age, deleteItem, toDetail}) => {
    return (
        <View style={styles.wrapper}>
            <View style={styles.cotainerImage}>
                <Image
                    source={{uri:urlAvatar}}
                    style={styles.imageProfile}
                />
            </View>
            <View style={styles.containerText}>
                <Text style={styles.textStyleName}>{firstName} {lastName}</Text>
                <Text style={styles.textStyleAge}>Age : {age} year(s)</Text>
            </View>
            <View style={styles.buttonGroup}>
                <TouchableOpacity onPress={toDetail}>
                    <Image
                        source={require('../../assets/images/button-edit.png')}
                        style={styles.buttonList}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={deleteItem}>
                    <Image
                        source={require('../../assets/images/button-delete.png')}
                        style={styles.buttonList}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper:{
        padding:10, 
        borderWidth:1, 
        borderColor:'brown', 
        marginHorizontal:10, 
        marginVertical:10, 
        borderRadius:15, 
        flexDirection:'row', 
        alignItems:'center'
    },
    imageProfile:{
        width:60, 
        height:60, 
        borderRadius:30
    },
    textStyleName:{
        fontSize:14, 
        fontWeight:'bold'
    },
    textStyleAge:{
        fontSize:11
    },
    containerText:{
        marginLeft:15, 
        flex : 2
    },
    cotainerImage:{
        flex:0.75
    },
    buttonList:{
        width:35, 
        height:35, 
        marginRight:10},
    buttonGroup:{
        flexDirection:'row', 
        flex:1
    },
    wrapperPopUp: {
        flex:1, 
        justifyContent:'center', 
        alignItems:'center', 
        marginTop:22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        width: 90,
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
})

export default ListContact;
