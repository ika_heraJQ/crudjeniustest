import axios from 'axios';
import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View, Keyboard, Modal, Pressable, Image, ScrollView } from 'react-native'
import { useRoute, useNavigation } from '@react-navigation/native'

const AddContact = () => {
    const navigation = useNavigation();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState('');
    const [photo, setPhoto] = useState('https://i.pravatar.cc/150?u=default');
    const [popUpVisible, setPopUpVisible] = useState(false);
    const [msgPopUp, setMsgPopUp] = useState('');
    const route = useRoute();
    console.log(route);
    console.log(route.params);

    useEffect(() => {
        setFirstName(route.name == 'UpdateContact' ? route.params.params.firstName : firstName)
        setLastName(route.name == 'UpdateContact' ? route.params.params.lastName : lastName)
        setAge(route.name == 'UpdateContact' ? route.params.params.age.toString() : age)
        setPhoto(route.name == 'UpdateContact' ? route.params.params.photo : photo)
    }, [])

    const submit = () => {
        const data = {
            firstName,
            lastName,
            age,
            photo
        }

        console.log('data submitted : ', data);
        if(route.name == 'UpdateContact'){
            axios.put(`https://simple-contact-crud.herokuapp.com/contact/${route.params.params.id}`, data)
            .then(res => {
                console.log('res post : ', res)
                setFirstName('')
                setLastName('')
                setAge('')
                Keyboard.dismiss()
                setPopUpVisible(true);
                setMsgPopUp(res.data.message.toString())
            })
            .catch(err => {
                console.log('err post : ', err)
                setPopUpVisible(true);
                setMsgPopUp(err.toString())
            })
        } else {
            axios.post('https://simple-contact-crud.herokuapp.com/contact', data)
            .then(res => {
                console.log('res post : ', res)
                setFirstName('')
                setLastName('')
                setAge('')
                Keyboard.dismiss()
                setPopUpVisible(true);
                setMsgPopUp(res.data.message.toString())
            })
            .catch(err => {
                console.log('err post : ', err)
                setPopUpVisible(true);
                setMsgPopUp(err.toString())
            })
        }
    }

    const generateNewAva = () => {
        var newString = "";
        var options = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 7; i++) {
            newString += options.charAt(Math.floor(Math.random() * options.length));
        }

        setPhoto('https://i.pravatar.cc/150?u=' + newString)
    }

    return (
        <ScrollView>
            <View style={styles.wrapper}>
                <View style={styles.imageWrapper}>
                    <Image
                        source={{uri:photo}}
                        style={styles.imageProfile}
                    />
                    <TouchableOpacity onPress={generateNewAva}>
                    <View style={styles.buttonStyleImage}>
                        <Text style={styles.textButtonStyleImage}>Change Ava</Text>
                    </View>
                    </TouchableOpacity>
                </View>
                <TextInput style={styles.textInputStyle} placeholder='First Name' value={firstName} onChangeText={(value) => setFirstName(value)}/>
                <TextInput style={styles.textInputStyle} placeholder='Last Name' value={lastName} onChangeText={(value) => setLastName(value)}/>
                <TextInput style={styles.textInputStyle} placeholder='Age' value={age} onChangeText={(value) => setAge(value)}/>
                <TouchableOpacity onPress={submit}>
                    <View style={styles.buttonStyle}>
                        <Text style={styles.textButtonStyle}>{route.name == 'UpdateContact' ? 'Update Contact' : 'Add Contact'}</Text>
                    </View>
                </TouchableOpacity>
                
            </View>
            <Modal
                animationType='fade'
                transparent={true}
                visible={popUpVisible}
                onRequestClose={
                    () => {
                        Alert.alert('Modal has been closed');
                        setPopUpVisible(!popUpVisible);
                    }
                }
            >
                <View style={styles.wrapperPopUp}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>{msgPopUp}</Text>
                        <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => {
                            setPopUpVisible(!popUpVisible)
                            navigation.navigate('ListContact');
                        }}
                        >
                        <Text style={styles.textStyle}>OK</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    wrapper:{
        padding:20
    },
    titleForm:{
        textAlign:'center', 
        fontSize:16, 
        fontWeight:'bold', 
        padding:20
    },
    textInputStyle:{
        borderColor:'brown', 
        borderWidth:1, 
        paddingLeft:15, 
        marginVertical:10, 
        borderRadius:10
    },
    buttonStyle:{
        backgroundColor:'brown', 
        padding:15, 
        marginVertical:20, 
        width:250, 
        alignSelf:'center', 
        borderRadius:25
    },
    buttonStyleImage:{
        backgroundColor:'lightblue',
        padding:5,
        marginVertical:5, 
        width:100, 
        alignSelf:'center', 
        borderRadius:25
    },
    textButtonStyleImage:{
        textAlign:'center', 
        fontSize:10, 
        color:'black', 
        fontWeight:'bold'
    },
    textButtonStyle:{
        textAlign:'center', 
        fontSize:16, 
        color:'white', 
        fontWeight:'bold'
    },
    wrapperPopUp: {
        flex:1, 
        justifyContent:'center', 
        alignItems:'center', 
        marginTop:22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        width: 90,
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    imageProfile:{
        width:100, 
        height:100, 
        borderRadius:50
    },
    imageWrapper:{
        alignItems:'center',
        marginBottom:20
    }
})

export default AddContact;